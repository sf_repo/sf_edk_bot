import telebot
from local_settings import TOKEN, keys
from extensions import ConversionException, CryptoConversion

bot = telebot.TeleBot(TOKEN)


# Обрабатываются все сообщения, содержащие команды '/start' or '/help'.
@bot.message_handler(commands=['start', 'help'])
def send_welcome(message):
    text = 'Для начала работы введите команду в формате\n\
<имя валюты, цену которой он хочет узнать>\n\
<имя валюты, в которой надо узнать цену первой валюты>\n\
<количество первой валюты>\n\
через пробел и в единственном числе.\n\n\
Список доступных валют: /values'
    bot.reply_to(message, f"Welcome, {message.chat.username},\n\n{text}")


@bot.message_handler(commands=['values'])
def values(message: telebot.types.Message):
    text = 'Доступные валюты:'
    for key in keys.keys():
        text = '\n'.join((text, key,))
    bot.reply_to(message, text)


@bot.message_handler(content_types=['text'])
def convert(message: telebot.types.Message):
    try:
        values = message.text.split(' ')

        if len(values) != 3:
            raise ConversionException('Неверное количество параметров')

        quote, base, amount = values
        total_base = CryptoConversion.get_price(quote, base, amount)
    except ConversionException as e:
        bot.reply_to(message, f'Ошибка пользователя\n{e}')
    except Exception as e:
        bot.reply_to(message, f'Не удалось обработать команду\n{e}')
    else:
        text = f'Цена {amount} {quote} в {base} - {total_base}'
        bot.reply_to(message, text)


# Обрабатывается все документы и аудиозаписи
@bot.message_handler(content_types=['document', 'audio'])
def handle_docs_audio(message):
    pass


# Обрабатывает фото
@bot.message_handler(content_types=['photo'])
def say_lmao(message: telebot.types.Message):
    bot.reply_to(message, 'Nice meme XDD')


bot.polling()
